from django.contrib.auth.models import User, UserManager
from django_cas.backends import CASBackend

class MyCASBackend(CASBackend):

    def populate_user(self, user, attributes):
        roles = attributes["roles"].split(",")
        user.email = attributes["email"]
        if "staff" in roles:
            user.is_staff = True
        if "admin" in roles:
            user.is_superuser = True
        return user
